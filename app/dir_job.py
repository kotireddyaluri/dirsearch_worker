from flask_rq2 import RQ
from rq import get_current_job
import urllib
import subprocess
import os


rq = RQ()
rq.redis_url = 'redis://redis:6379'
#rq.redis_url = 'redis://localhost:6379'

@rq.job(timeout=604800)
def dirsearch_v1(sc_host):
    print("I am reached")
    sc_hostname = gethostname(sc_host)
    self_job = get_current_job()
    dirsearch_v1_cmd = subprocess.run([dirsearch_cmd_path,"-b","-u",sc_host,"-e","php,asp,aspx,jsp,html,htm,js,json","-t","60","--skip-on-status=429"])
    return "completed dirsearch for "+sc_host

def gethostname(sc_host):
    sc_hostname=sc_host
    if sc_host.startswith('http:') or sc_host.startswith('https:'):
        sc_hostname=urllib.parse.urlparse(sc_host).hostname
    return sc_hostname

homedir= os.environ['HOME']

dirsearch_cmd_path='/dirsearch/dirsearch.py'
#dirsearch_cmd_path=homedir+'/sectools/dirsearch/dirsearch.py'