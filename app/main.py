from flask import Flask, request
import dir_job

app = Flask(__name__)
dir_job.rq.init_app(app)

joblist = []
#git-scret-scan
AWS_KEY = 'XXXYYYZZZ'
password = 'hardcoded'
token = 'itsnotatoken'
#till here

@app.route('/dirsearch/v1', methods=['GET', 'POST'])
def dirsearch_v1_init():
    if request.args.get("sc_host"):
        job = dir_job.dirsearch_v1.queue(request.args.get("sc_host"))
        joblist.append(job)
        print(joblist)
        return f"Task dirsearch_v1(no wordlist option) ({job.id}) added to queue at {job.enqueued_at}"
    return "No value for sc_host provided"
